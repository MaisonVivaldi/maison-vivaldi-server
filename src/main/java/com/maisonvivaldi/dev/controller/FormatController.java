package com.maisonvivaldi.dev.controller;

import com.maisonvivaldi.dev.model.Format;
import com.maisonvivaldi.dev.repositories.FormatRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class FormatController {

    private final FormatRepository formatRepository;

    @Autowired
    public FormatController(FormatRepository formatRepository) {
        this.formatRepository = formatRepository;

    }

    @GetMapping("/formats")
    @CrossOrigin
    ResponseEntity<List<Format>> getAllFormats() {
        List<Format> formats = formatRepository.findAll();
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(formats);
    }

    @PostMapping("/adminFormats")
    @CrossOrigin
    ResponseEntity<Format> createFormats(@RequestBody Format mFormat){
        // todo check that the user is an admin
        formatRepository.save(mFormat);
        Format savedFormat = formatRepository.findFormatByFormatName(mFormat.getFormatName());
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(savedFormat);
    }

    @DeleteMapping("/adminFormats/{formatName}")
    @CrossOrigin
    ResponseEntity<Format> deleteFormats(@PathVariable(value = "formatName") String formatName){
        // todo check that the user is an admin
        Format mFormat = this.formatRepository.findFormatByFormatName(formatName);
        this.formatRepository.delete(mFormat);
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(mFormat);
    }
}
