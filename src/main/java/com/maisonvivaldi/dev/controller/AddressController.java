package com.maisonvivaldi.dev.controller;


import com.maisonvivaldi.dev.model.Address;
import com.maisonvivaldi.dev.model.JwtUserDetails;
import com.maisonvivaldi.dev.model.User;
import com.maisonvivaldi.dev.repositories.UserRepository;
import com.maisonvivaldi.dev.service.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class AddressController {

    private final UserRepository userRepository;
    private final AddressService addressService;

    @Autowired
    public AddressController(
            UserRepository userRepository,
            AddressService addressService) {
        this.userRepository = userRepository;
        this.addressService = addressService;
    }

    @CrossOrigin
    @GetMapping("/address")
    ResponseEntity<?> getAllAddresses() {
        JwtUserDetails userDetails = (JwtUserDetails) SecurityContextHolder.getContext()
                .getAuthentication().getPrincipal();
        String userName = userDetails.getUsername();
        User user = userRepository.findByUsername(userName);
        return addressService.checkAllAddressUser(user);
    }

    @CrossOrigin
    @GetMapping("/address/user")
    ResponseEntity<?> getAllUserAddress(){
        JwtUserDetails userDetails = (JwtUserDetails) SecurityContextHolder.getContext()
                .getAuthentication().getPrincipal();
        String userName = userDetails.getUsername();
        User user = userRepository.findByUsername(userName);
        return addressService.getAllAddressByUser(user);
    }

    @CrossOrigin
    @GetMapping("/address/{id}")
    ResponseEntity<?> getAddressById(@PathVariable int id) {
        JwtUserDetails userDetails = (JwtUserDetails) SecurityContextHolder.getContext()
                .getAuthentication().getPrincipal();
        String userName = userDetails.getUsername();
        User user = userRepository.findByUsername(userName);
        return addressService.checkAddressById(user, id);
    }

    @CrossOrigin
    @PostMapping("/address")
    ResponseEntity<Address> createAddress(@RequestBody Address mAddress) {
        JwtUserDetails userDetails = (JwtUserDetails) SecurityContextHolder.getContext()
                .getAuthentication().getPrincipal();
        String userName = userDetails.getUsername();
        User user = userRepository.findByUsername(userName);
        return addressService.createAddress(mAddress, user);
    }

    @CrossOrigin
    @PutMapping("/address/{address}")
    ResponseEntity<?> updateAddress(@PathVariable(value = "address") int addressId,
                                    @RequestBody Address mAddress) {
        JwtUserDetails userDetails = (JwtUserDetails) SecurityContextHolder.getContext()
                .getAuthentication().getPrincipal();
        String userName = userDetails.getUsername();
        User user = userRepository.findByUsername(userName);
        return addressService.updateAddress(addressId, mAddress, user);
    }

    @CrossOrigin
    @DeleteMapping("/address/{address}")
    ResponseEntity<String> deleteAddress(@PathVariable(value = "address") int addressId) {
        JwtUserDetails userDetails = (JwtUserDetails) SecurityContextHolder.getContext()
                .getAuthentication().getPrincipal();
        String userName = userDetails.getUsername();
        User user = userRepository.findByUsername(userName);
        return addressService.deleteAddress(addressId, user);
    }

}
