package com.maisonvivaldi.dev.controller;

import com.maisonvivaldi.dev.service.StripeClient;
import com.stripe.model.Refund;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@Controller
public class RefundController {

    @Autowired
    private final StripeClient stripeClient;

    RefundController(StripeClient stripeClient){
        this.stripeClient = stripeClient;
    }

   /*@CrossOrigin
    @PostMapping("/refund")
    ResponseEntity<String> refundCommand(@RequestBody String paymentId){
        this.stripeClient.refundUserCommand(paymentId);
      return ResponseEntity
              .status(HttpStatus.OK)
              .body("SUCCESS TO REFUND ");
    }*/
}
