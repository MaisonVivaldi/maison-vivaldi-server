package com.maisonvivaldi.dev.controller;

import com.maisonvivaldi.dev.model.stripeModel.Amount;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.maisonvivaldi.dev.service.StripeClient;

@RestController
@RequestMapping("/payment")
public class PaymentController {

    private final StripeClient stripeClient;

    @Autowired
    PaymentController(StripeClient stripeClient) {
        this.stripeClient = stripeClient;
    }

    @CrossOrigin
    @PostMapping("/charge")
    public String chargeCard(@RequestBody Amount amountValue) throws Exception {
        return this.stripeClient.paymentIntentCreate(amountValue.getAmount(), amountValue.getToken());
    }

}
