package com.maisonvivaldi.dev.controller;

import com.maisonvivaldi.dev.model.Command;
import com.maisonvivaldi.dev.model.JwtUserDetails;
import com.maisonvivaldi.dev.repositories.CommandRepository;
import com.maisonvivaldi.dev.service.CommandService;
import com.maisonvivaldi.dev.service.EmailService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@Slf4j
public class CommandController {

    private final CommandService commandService;
    private final CommandRepository commandRepository;

    private EmailService service;

    @Autowired
    public CommandController(CommandService commandService, CommandRepository commandRepository,EmailService service) {
        this.commandService = commandService;
        this.commandRepository = commandRepository;
        this.service = service;
    }

    @CrossOrigin
    @PostMapping("/command")
    ResponseEntity<?> command(@RequestBody Command mCommand) {
        JwtUserDetails userDetails = (JwtUserDetails) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        try {
            ResponseEntity<?> responseEntity = this.commandService.saveCommand(mCommand, userDetails);
            service.sendEmail(mCommand, userDetails.getUsername());
            return responseEntity;
        } catch (Exception e) {
            log.info(CommandController.class + ": command error ");
            return ResponseEntity
                    .status(HttpStatus.CREATED)
                    .body(CommandController.class + ": command error");
        }
    }

    @CrossOrigin
    @GetMapping("/allCommand")
    ResponseEntity<?> listUserCommand() {
        JwtUserDetails userDetails = (JwtUserDetails) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        userDetails.getAuthorities();
        if (userDetails.getUsername().equals("admin")) {
            return this.commandService.getAllCommand();
        } else {
            return ResponseEntity
                    .status(HttpStatus.CREATED)
                    .body("not authorized");
        }
    }

    @CrossOrigin
    @GetMapping("/command/{idInvoice}")
    ResponseEntity<?> getCommand(@PathVariable String idInvoice) {
        JwtUserDetails userDetails = (JwtUserDetails) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        try {
            return this.commandService.getCommandById(userDetails, idInvoice);
        } catch (Exception e) {
            log.info(CommandController.class + ": getCommand error " + idInvoice);
            return ResponseEntity
                    .status(HttpStatus.CREATED)
                    .body(CommandController.class + ": getCommand error " + idInvoice);
        }
    }

    @CrossOrigin
    @GetMapping("/allUserCommand")
    ResponseEntity<List<Command>> getUserCommands() {
        JwtUserDetails userDetails = (JwtUserDetails) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        return this.commandService.getAllUserCommand(userDetails);
    }

    @CrossOrigin
    @GetMapping("/command/{invoiceId}/{status}")
    ResponseEntity<?> updateStatus(@PathVariable String invoiceId,  @PathVariable String status){
        JwtUserDetails userDetails = (JwtUserDetails) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();

        Command command = this.commandRepository.findCommandByInvoiceId(invoiceId);
        return this.commandService.udpateStatus(command, status);
    }


}
