package com.maisonvivaldi.dev.controller;

import com.maisonvivaldi.dev.model.ImageContent;
import com.maisonvivaldi.dev.service.ImageColorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class ColorImageController {

    ImageColorService imageColorService;

    @Autowired
    public ColorImageController(ImageColorService imageColorService) {
        this.imageColorService = imageColorService;
    }

    @CrossOrigin
    @PostMapping("/get_image_color")
    ResponseEntity<?> getImageColor(@RequestBody ImageContent imageContent) {
        try {
            Map<String, Integer> mapContent = imageColorService.getImageColor(imageContent);
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(mapContent);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(CommandController.class + ": command error");
        }
    }
}
