package com.maisonvivaldi.dev.controller;

import com.maisonvivaldi.dev.model.JwtUserDetails;
import com.maisonvivaldi.dev.model.TokenVerification;
import com.maisonvivaldi.dev.model.User;
import com.maisonvivaldi.dev.repositories.TokenVerificationRepository;
import com.maisonvivaldi.dev.repositories.UserRepository;
import com.maisonvivaldi.dev.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
public class EmailVerificationController {



    private final UserRepository userRepository;
    private final UserService userService;
    private final TokenVerificationRepository tokenVerificationRepository;

    @Autowired
    EmailVerificationController(TokenVerificationRepository tokenVerificationRepository, UserRepository userRepository, UserService userService) {
        this.userService = userService;
        this.userRepository = userRepository;
        this.tokenVerificationRepository = tokenVerificationRepository;
    }

    @CrossOrigin
    @GetMapping("/confirm-account/{token}")
    ResponseEntity<String> confirmUserAccount(@PathVariable String token) {
        JwtUserDetails userDetails = (JwtUserDetails) SecurityContextHolder.getContext()
                .getAuthentication().getPrincipal();
        String username = userDetails.getUsername();
        User user = userRepository.findByUsername(username);
        return userService.confirmUser(token, user);


    }
}
