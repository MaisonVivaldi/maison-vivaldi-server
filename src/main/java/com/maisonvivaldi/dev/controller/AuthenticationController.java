package com.maisonvivaldi.dev.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.maisonvivaldi.dev.config.Encryption;
import com.maisonvivaldi.dev.config.JwtCheckingService;
import com.maisonvivaldi.dev.model.JwtUserDetails;
import com.maisonvivaldi.dev.model.TokenVerification;
import com.maisonvivaldi.dev.model.User;
import com.maisonvivaldi.dev.repositories.TokenVerificationRepository;
import com.maisonvivaldi.dev.repositories.UserRepository;
import com.maisonvivaldi.dev.service.EmailService;
import com.maisonvivaldi.dev.service.TokenVerificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

@RestController
public class AuthenticationController {

    private final UserRepository userRepository;
    private final JwtCheckingService jwtCheckingService;
    private final TokenVerificationRepository tokenVerificationRepository;
    private final EmailService emailService;
    private final Gson gson;
    private final Encryption passwordEncoder;

    @Autowired
    public AuthenticationController(UserRepository userRepository, EmailService emailService, JwtCheckingService jwtCheckingService,
                                    TokenVerificationService tokenVerificationService, TokenVerificationRepository tokenVerificationRepository, Encryption passwordEncoder) {
        this.userRepository = userRepository;
        this.jwtCheckingService = jwtCheckingService;
        this.emailService = emailService;
        this.tokenVerificationRepository = tokenVerificationRepository;


        this.gson = new GsonBuilder()
                .excludeFieldsWithoutExposeAnnotation()
                .create();
        this.passwordEncoder = passwordEncoder;
    }

    @CrossOrigin
    @PostMapping("/register")
    ResponseEntity<String> register(@RequestBody User mUser){
        try {
            if(mUser.getPassword() == null){
                throw new Exception();
            }

            boolean exists = userRepository.existsUserByUsername(mUser.getUsername());
            if (exists) {
                throw new DuplicateKeyException("duplicata");
            }
            String password = mUser.getPassword();
            mUser.setPassword((passwordEncoder.passwordEncoder()).encode(mUser.getPassword()));
            userRepository.save(mUser);
            User sUser = userRepository.findByUsername(mUser.getUsername());
            jwtCheckingService.setupUser(mUser.getUsername(), password);
            String mToken = jwtCheckingService.getValidToken();
            ObjectMapper mapper = new ObjectMapper();
            HashMap<String, String> result = new HashMap<String, String>();
            result.put("username", sUser.getUsername());
            result.put("token", mToken);
            String jsonString = mapper.writeValueAsString(result);
            TokenVerification tokenVerification = new TokenVerification(mUser);
            tokenVerificationRepository.save(tokenVerification);
            emailService.SendConfirmationMail(mUser.getUsername(), tokenVerification.getToken());
            return ResponseEntity
                    .status(HttpStatus.CREATED)
                    .body(jsonString);
        }catch (DuplicateKeyException e){
            return ResponseEntity
                    .status(HttpStatus.CONFLICT)
                    .body("{ \"error\": \"duplicata\" }");
        }
        catch (Exception e){
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body("{ \"error\": \"other error\" }");
        }
    }

    @CrossOrigin
    @PostMapping("/authenticate")
    ResponseEntity<String> authenticate(@RequestBody User mUser){
        try {
            jwtCheckingService.setupUser(mUser.getUsername(),
                    mUser.getPassword());
            String mToken = jwtCheckingService.getValidToken();
            return ResponseEntity
                    .status(HttpStatus.CREATED)
                    .body("{ \"token\" : \""+mToken+"\"}");
        }catch (Exception ex){
            return ResponseEntity
                    .status(HttpStatus.UNAUTHORIZED)
                    .body("Mauvais utilisateur / mot de passe");
        }
    }

    @CrossOrigin
    @GetMapping("/me")
    public ResponseEntity<String> me() {
        try {
            JwtUserDetails userDetails = (JwtUserDetails) SecurityContextHolder.getContext()
                    .getAuthentication().getPrincipal();
            String userName = userDetails.getUsername();
            User user = userRepository.findByUsername(userName);
            String result = gson.toJson(user);
            return ResponseEntity
                    .status(HttpStatus.CREATED)
                    .body(result);
        } catch (Exception ex) {
            ex.printStackTrace();
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body("{ \"error\": \"error\" }");
        }
    }

}
