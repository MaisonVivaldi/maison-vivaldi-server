package com.maisonvivaldi.dev.controller;


import com.maisonvivaldi.dev.model.JwtUserDetails;
import com.maisonvivaldi.dev.model.User;
import com.maisonvivaldi.dev.repositories.UserRepository;
import com.maisonvivaldi.dev.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class UserController {

    private final UserRepository userRepository;
    private final UserService userService;


    @Autowired
    public UserController(UserRepository userRepository, UserService userService) {
        this.userRepository = userRepository;
        this.userService = userService;
    }

    @GetMapping("/user")
    ResponseEntity<?> getAllUsers() {
        JwtUserDetails userDetails = (JwtUserDetails) SecurityContextHolder.getContext()
                .getAuthentication().getPrincipal();
        String userName = userDetails.getUsername();
        User user = userRepository.findByUsername(userName);
        return userService.checkAllUsers(user);
    }

    @CrossOrigin
    @GetMapping("/user/{id}")
    ResponseEntity<String> getUserById(@PathVariable int id) {
        JwtUserDetails userDetails = (JwtUserDetails) SecurityContextHolder.getContext()
                .getAuthentication().getPrincipal();
        String userName = userDetails.getUsername();
        User user = userRepository.findByUsername(userName);
        return userService.checkUserById(user, id);
    }

    @CrossOrigin
    @GetMapping("/user/me")
    ResponseEntity<User> getUserByUsername(){
        JwtUserDetails userDetails = (JwtUserDetails) SecurityContextHolder.getContext()
                .getAuthentication().getPrincipal();
        String userName = userDetails.getUsername();
        return userService.getUserByUsername(userName);
    }

    @CrossOrigin
    @PutMapping("/user/{user}")
    ResponseEntity<?> updateUser(@PathVariable(value = "user") int userId,
                                 @RequestBody User mUser) {
        JwtUserDetails userDetails = (JwtUserDetails) SecurityContextHolder.getContext()
                .getAuthentication().getPrincipal();
        String userName = userDetails.getUsername();
        User user = userRepository.findByUsername(userName);
        return userService.updateUser(userId, mUser, user);
    }

    @CrossOrigin
    @GetMapping("/alluser")
    ResponseEntity<?> listAlluser() {
        JwtUserDetails userDetails = (JwtUserDetails) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        userDetails.getAuthorities();
        if (userDetails.getUsername().equals("admin")) {
            return this.userService.getAllUser();
        } else {
            return ResponseEntity
                    .status(HttpStatus.CREATED)
                    .body("not authorized");
        }
    }
}
