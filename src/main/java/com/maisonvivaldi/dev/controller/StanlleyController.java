package com.maisonvivaldi.dev.controller;

import com.maisonvivaldi.dev.model.StanlleyAllProducts;
import com.maisonvivaldi.dev.repositories.StanlleyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@Controller
public class StanlleyController {

    @Autowired
    private final StanlleyRepository stanlleyRepository;

    @Autowired
    public StanlleyController(StanlleyRepository stanlleyRepository) {
        this.stanlleyRepository = stanlleyRepository;
    }

    @CrossOrigin
    @GetMapping("/stanlleyProducts/{genderCat}")
    ResponseEntity<List<StanlleyAllProducts>> getProductsByGenre(@PathVariable String genderCat) {
        List<StanlleyAllProducts> stanlleyAllProducts;
        if (!genderCat.equals("accessoires")) {
            stanlleyAllProducts = this.stanlleyRepository.findStanlleyAllProductsByGenderAndCategoryNot(genderCat,
                    "accessoires");
        } else {
            stanlleyAllProducts = this.stanlleyRepository.findStanlleyAllProductsByCategory(genderCat);
        }

        Collections.sort(stanlleyAllProducts, new Comparator<StanlleyAllProducts>() {
            @Override
            public int compare(StanlleyAllProducts pro1, StanlleyAllProducts pro2) {
                return Boolean.compare(!pro1.isNewStyle(), !pro2.isNewStyle());
            }
        });

        return ResponseEntity
                .status(HttpStatus.OK)
                .body(stanlleyAllProducts);
    }

    @CrossOrigin
    @GetMapping("/stanlleyProducts")
    ResponseEntity<List<StanlleyAllProducts>> getAllProducts() {
        List<StanlleyAllProducts> stanlleyAllProducts = this.stanlleyRepository.findAll();
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(stanlleyAllProducts);
    }

    @CrossOrigin
    @GetMapping("/stanlleyProduct/{styleCode}")
    ResponseEntity<List<StanlleyAllProducts>> getSingleProduct(@PathVariable String styleCode) {
        List<StanlleyAllProducts> stanlleyAllProducts =
                this.stanlleyRepository.findStanlleyAllProductsByStyleCode(styleCode);
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(stanlleyAllProducts);
    }
}
