package com.maisonvivaldi.dev.model.email;

import lombok.Data;

@Data
public class MailRequest {

    private String to;
    private String from;
    private String subject;

}
