package com.maisonvivaldi.dev.model;


import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class Delivery {

    @Id
    @GeneratedValue
    private int id;

    private String productTime;

    @OneToOne(cascade = CascadeType.ALL)
    private DestinationPerson destinationPerson;

    @OneToOne(cascade = CascadeType.ALL)
    private Command command;

    private String dhlDelivery;
    private Boolean packing;
}
