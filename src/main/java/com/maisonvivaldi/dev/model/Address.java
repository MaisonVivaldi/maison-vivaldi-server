package com.maisonvivaldi.dev.model;



import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

@Entity
@Data
public class Address {


    @Id
    @GeneratedValue
    int id;

    @Column(length = 100, nullable = false)
    String street;

    @Column(name = "postal_code", length = 30, nullable = false)
    String postalCode;

    @Column(length = 50, nullable = false)
    String city;

    @Column(length = 50, nullable = false)
    String country;

    @Column(name = "creation_date")
    @CreationTimestamp
    Date creationDate;

    @Column(name = "update_date")
    @UpdateTimestamp
    Date updateDate;

    @ManyToOne
    @JsonIgnore
    User user;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Address address = (Address) o;
        return street.equals(address.street) &&
                postalCode.equals(address.postalCode) &&
                city.equals(address.city) &&
                country.equals(address.country);
    }

    @Override
    public int hashCode() {
        return Objects.hash(street, postalCode, city, country);
    }
}
