package com.maisonvivaldi.dev.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Data
@Entity
public class LogoPlusProductImages {

    @Id
    @GeneratedValue
    private int id;

    private String logoPlusProductImages;
}
