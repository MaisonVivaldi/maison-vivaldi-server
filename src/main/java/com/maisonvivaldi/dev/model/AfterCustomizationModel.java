package com.maisonvivaldi.dev.model;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
public class AfterCustomizationModel {

    @Id
    @GeneratedValue
    private int id;

    @OneToMany(cascade = CascadeType.ALL)
    private List<Logos> logos;

    @OneToOne
    Product product;
}
