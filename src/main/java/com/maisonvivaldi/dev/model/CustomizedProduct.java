package com.maisonvivaldi.dev.model;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
public class CustomizedProduct {

    @Id
    @GeneratedValue
    private int id;

    private String logoPlusProduct;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id")
    private Product product;

    @OneToMany(cascade = CascadeType.ALL,orphanRemoval = true)
    private List<Logos> logo;
}
