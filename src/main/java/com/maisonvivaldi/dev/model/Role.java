package com.maisonvivaldi.dev.model;

public enum Role {
    ROLE_USER,
    ROLE_ADMIN
}
