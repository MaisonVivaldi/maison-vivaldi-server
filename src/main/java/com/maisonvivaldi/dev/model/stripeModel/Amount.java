package com.maisonvivaldi.dev.model.stripeModel;

import lombok.Data;
import org.springframework.stereotype.Component;

@Data
@Component
public class Amount {
    private int amount;
    private String token;
}
