package com.maisonvivaldi.dev.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@Data
public class Alignment {

    @Id
    @GeneratedValue
    private int id;

    private int xCord;
    private int yCord;
    private int width;
    private int height;
    private int xCordWidth;
    private int yCordWidth;

}
