package com.maisonvivaldi.dev.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Data
@Entity
public class Format {

    @Id
    @GeneratedValue
    int id;

    @Column(unique=true, nullable = false)
    private String formatName;

    private int width;

    private int height;

}
