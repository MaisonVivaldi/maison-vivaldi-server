package com.maisonvivaldi.dev.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class StanlleyAllProducts {

    @Id
    @JsonProperty("B2BSKUREF")
    private String id;

    @JsonProperty("StyleCode")
    private String styleCode;

    @JsonProperty("ColorCode")
    private String colorCode;

    @JsonProperty("SizeCode")
    private String sizeCode;

    @JsonProperty("StyleName")
    private String styleName;

    @JsonProperty("Color")
    private String color;

    @JsonProperty("Type")
    private String type;

    @JsonProperty("Category")
    private String category;

    @JsonProperty("Gender")
    private String gender;

    @JsonProperty("Stock")
    private String stock;

    @JsonProperty("Fit")
    private String fit;

    @JsonProperty("ShortDescription")
    private String shortDescription;

    @JsonProperty("ShortNote")
    private String shortNote;

    @JsonProperty("Bleaching")
    private String bleaching;

    @JsonProperty("Washing")
    private String washing;

    @JsonProperty("Cleaning")
    private String cleaning;

    @JsonProperty("Drying")
    private String drying;

    @JsonProperty("Ironing")
    private String ironing;

    @JsonProperty("CompositionList")
    private String compositionList;

    @JsonProperty("Weight")
    private String weight;

    @JsonProperty("Medium Brand EUR")
    private String medium_brand_eur;

    @JsonProperty("NewStyle")
    private boolean newStyle;

    @JsonProperty("NewProduct")
    private boolean newProduct;

    @JsonProperty("MainPicture")
    private String mainPicture;

    @JsonProperty("VEGAN")
    private String vegan;
}
