package com.maisonvivaldi.dev.model;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Data
@Entity
public class User {

    @Id
    @GeneratedValue
    private int id;

    private String stripeId;

    @Column(name = "username", unique = true, nullable = false)
    private String username;

    @Column(nullable = false)
    private String password;

    private boolean isVerified;

    @Column(name = "role", columnDefinition = "varchar(255) default 'ROLE_USER'")
    @Enumerated(EnumType.STRING)
    private Role role = Role.ROLE_USER;

    @CreationTimestamp
    private Date creationDate;

    @UpdateTimestamp
    private Date updatedDate;

    @OneToMany(mappedBy = "user")
    List<Address> addressesList;

    @OneToMany(mappedBy = "mUser",
            cascade = CascadeType.ALL,
            orphanRemoval = true,
            fetch = FetchType.LAZY)
    List<Command> userCommandList;

    public void setAddress(Address mAddress) {
        this.addressesList.add(mAddress);
    }
}
