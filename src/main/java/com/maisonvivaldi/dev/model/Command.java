package com.maisonvivaldi.dev.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
public class Command {

    @Id
    @GeneratedValue
    private int id;

    private String invoiceId;

    @OneToMany(cascade = CascadeType.ALL)
    private List<ListSelectedProducts> listSelectedProducts;

    @OneToOne(cascade = CascadeType.ALL)
    private Prices prices;

    @OneToOne(cascade = CascadeType.ALL)
    private Delivery deliver;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnore
    private User mUser;

    private String webCommandStatus;

    private String issueDate;

    private String deliveryStatus;

    private String paymentId;
}
