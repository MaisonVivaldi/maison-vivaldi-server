package com.maisonvivaldi.dev.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class Prices {
    @Id
    @GeneratedValue
    private int id;

    float totalPriceImpNum;
    float totalPriceSer;
    float totalPriceTVAImpNum;
    float totalPriceTVASer;
    float totalPriceWithoutTVAImpNum;
    float totalPriceWithoutTVASer;
    float deliveryPrice;
    float totalPriceWithDeliveryImpN;
    float totalPriceWithDeliverySer;
    String selectedTechnique;
    boolean autoSelection;

    @OneToOne
    private Command command;

}
