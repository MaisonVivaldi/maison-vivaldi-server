package com.maisonvivaldi.dev.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Data
@Entity
public class Logos {

    @Id
    @GeneratedValue
    private int logosId;

    private String logoContent;

    @ManyToOne
    private AfterCustomizationModel afterCustomizationModel;
}
