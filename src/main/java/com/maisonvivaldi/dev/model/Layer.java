package com.maisonvivaldi.dev.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class Layer {
    @Id
    @GeneratedValue
    private int id;

    private boolean hide_show;
    private String textContent;
    private String fontsStyle;
    private int fontsSize;
    private String fontsColor;
    private int type;

    @OneToOne
    private Alignment alignment;
}
