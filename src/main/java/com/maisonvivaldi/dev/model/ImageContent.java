package com.maisonvivaldi.dev.model;

import lombok.Data;

@Data
public class ImageContent {
    String imageContent;
    String imageSize;
}
