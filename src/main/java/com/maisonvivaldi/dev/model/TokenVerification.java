package com.maisonvivaldi.dev.model;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Data
@Entity
public class TokenVerification {

    @Id
    @GeneratedValue
    private int id;

    @Column(nullable = false)
    private String token;

    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;

    @OneToOne(targetEntity = User.class, fetch = FetchType.EAGER)
    @JoinColumn(nullable = false, name = "user_id")
    private User user;

    public  TokenVerification(User user){
        this.user = user;
        createdAt = new Date();
        token = UUID.randomUUID().toString();


    }

    public TokenVerification() {}
}
