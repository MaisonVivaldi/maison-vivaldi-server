package com.maisonvivaldi.dev.model;


import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Data
@Entity
public class DestinationPerson {

    @Id
    @GeneratedValue
    private int id;

    String nom;
    String societe;
    String prenom;
    String address;
    String pays = "France";
    String codePostal;
    String ville;

    @OneToOne
    Delivery delivery;
}
