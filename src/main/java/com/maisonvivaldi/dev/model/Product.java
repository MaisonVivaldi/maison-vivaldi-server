package com.maisonvivaldi.dev.model;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
public class Product {

    @Id
    @GeneratedValue
    private int id;

    private String selectedColor;
    private String pfmImage;
    private int totalQuantity;
    private int productPriceWithoutTva;
    private String styleCode;

    @OneToOne(cascade = CascadeType.ALL)
    private StanlleyProductDetails product;

    @ElementCollection
    private List<String> selectedSizes;

    @ElementCollection
    private List<String> sizesSelection;

    @ElementCollection
    private List<String> backFrontImages;

    @OneToMany(cascade = CascadeType.ALL)
    private List<Layer> backLayers;

    @OneToMany(cascade = CascadeType.ALL)
    private List<Layer> frontLayers;

//    private String listRotationDegree;

//    @ElementCollection
//    private List<String> detectedFormat;

    @OneToOne(cascade = CascadeType.ALL)
    private AfterCustomizationModel afterCustomization;

}
