package com.maisonvivaldi.dev.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class ListSelectedProducts {

    @Id
    @GeneratedValue
    int id;

    private String productStyleCode;

    @OneToOne(cascade = CascadeType.ALL)
    private Product productColor;

    private String logosPlusProductBack;

    private String logosPlusProductFront;

    @ManyToOne
    Command command;
}
