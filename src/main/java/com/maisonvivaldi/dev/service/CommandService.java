package com.maisonvivaldi.dev.service;

import com.maisonvivaldi.dev.model.*;
import com.maisonvivaldi.dev.repositories.CommandRepository;
import com.maisonvivaldi.dev.repositories.StanlleyRepository;
import com.maisonvivaldi.dev.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommandService {

    private final AwsImageServices awsImageServices;
    private final UserRepository userRepository;
    private final CommandRepository commandRepository;
    private final StanlleyRepository stanlleyRepository;

    @Autowired
    public CommandService(AwsImageServices awsImageServices, UserRepository userRepository,
                          CommandRepository commandRepository, StanlleyRepository stanlleyRepository) {
        this.awsImageServices = awsImageServices;
        this.userRepository = userRepository;
        this.commandRepository = commandRepository;
        this.stanlleyRepository = stanlleyRepository;
    }

    public ResponseEntity<Command> saveCommand(Command mCommand, JwtUserDetails userDetails) {
        for (ListSelectedProducts products : mCommand.getListSelectedProducts()) {
            updateStocks(mCommand);
            saveImagesToAws(products, userDetails);
        }
        User mUser = this.userRepository.findByUsername(userDetails.getUsername());
        mUser.getUserCommandList().add(mCommand);
        mCommand.setMUser(mUser);
        this.userRepository.save(mUser);
        Command savedCommand = commandRepository.findCommandByInvoiceId(mCommand.getInvoiceId());
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(savedCommand);
    }

    public ResponseEntity<List<Command>> getAllUserCommand(JwtUserDetails userDetails) {
        User mUser = userRepository.findByUsername(userDetails.getUsername());
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(mUser.getUserCommandList());
    }

    public ResponseEntity<List<Command>> getAllCommand() {
        List<Command> lCommand = commandRepository.findAll();
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(lCommand);
    }

    public ResponseEntity<?> getCommandById(JwtUserDetails userDetails, String invoiceId) {
        User mUser = userRepository.findByUsername(userDetails.getUsername());
        for (Command mCommand : mUser.getUserCommandList()) {
            if (mCommand.getInvoiceId().equals(invoiceId)) {
                return ResponseEntity
                        .status(HttpStatus.CREATED)
                        .body(mCommand);
            }
        }
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body("not found");
    }

    private void saveImagesToAws(ListSelectedProducts products, JwtUserDetails userDetails) {
        if (products.getLogosPlusProductBack() != null) {
            products.setLogosPlusProductBack(this.awsImageServices.uploadFile(products.getLogosPlusProductBack(),
                    userDetails.getUsername()));
        }
        if (products.getLogosPlusProductFront() != null) {
            products.setLogosPlusProductFront(this.awsImageServices.uploadFile(products.getLogosPlusProductFront(),
                    userDetails.getUsername()));
        }
        for (Logos logos : products.getProductColor().getAfterCustomization().getLogos()) {
            logos.setLogoContent(this.awsImageServices.uploadFile(logos.getLogoContent(),
                    userDetails.getUsername()));
        }
    }

    public void updateStocks(Command mCommand) {
        List<StanlleyAllProducts> stanlleyAllProducts = null;
        for (ListSelectedProducts listSelectedProducts : mCommand.getListSelectedProducts()) {
            int indexSize = 0;
            for (String quantity : listSelectedProducts.getProductColor().getSelectedSizes()) {
                stanlleyAllProducts = stanlleyRepository.findStanlleyAllProductsByStyleCodeAndColorCode(
                        listSelectedProducts.getProductColor().getStyleCode(),
                        listSelectedProducts.getProductColor().getSelectedColor());
                int quantityNumber = Integer.parseInt(stanlleyAllProducts.get(indexSize).getStock())
                        - Integer.parseInt(quantity);
                stanlleyAllProducts.get(indexSize).setStock(quantityNumber + "");
                indexSize++;
            }
            assert stanlleyAllProducts != null;
            this.stanlleyRepository.saveAll(stanlleyAllProducts);
        }

    }
    public ResponseEntity<?> udpateStatus(Command command, String status){

        if(command != null){
            command.setWebCommandStatus(status);
            command.setDeliveryStatus(status);
            this.commandRepository.save(command);
        }
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(command);
    }

}
