package com.maisonvivaldi.dev.service;

import com.maisonvivaldi.dev.model.Command;
import com.maisonvivaldi.dev.model.email.MailResponse;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;


@Service
public class EmailService {

    @Value("${spring.mail.username}")
    String source;
    @Autowired
    private JavaMailSender sender;
    @Autowired
    private Configuration config;

    public void sendEmail(Command mCommand, String destination) {
        Map<String, Object> model = new HashMap<>();
        model.put("command", mCommand);
        if (mCommand.getPrices().getSelectedTechnique().equals("ser")) {
            model.put("totalPriceWithDelivery", mCommand.getPrices().getTotalPriceWithDeliverySer());
            model.put("totalPriceTVA", mCommand.getPrices().getTotalPriceTVASer());
            model.put("totalPriceWithoutTVA", mCommand.getPrices().getTotalPriceWithoutTVASer());
        } else {
            model.put("totalPriceWithDelivery", mCommand.getPrices().getTotalPriceWithDeliveryImpN());
            model.put("totalPriceTVA", mCommand.getPrices().getTotalPriceTVAImpNum());
            model.put("totalPriceWithoutTVA", mCommand.getPrices().getTotalPriceWithoutTVAImpNum());
        }
        model.put("email", destination);

        MailResponse response = new MailResponse();
        MimeMessage message = sender.createMimeMessage();
        try {
            MimeMessageHelper helper = new MimeMessageHelper(message, MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
                    StandardCharsets.UTF_8.name());

            Template t = config.getTemplate("email-template.ftl");
            String html = FreeMarkerTemplateUtils.processTemplateIntoString(t, model);

            helper.setTo(destination);
            helper.setText(html, true);
            helper.setSubject("Command Bien Recu");
            helper.setFrom(source);
            sender.send(message);

            response.setMessage("mail send to : success");
            response.setStatus(Boolean.TRUE);

        } catch (MessagingException | IOException | TemplateException e) {
            response.setMessage("Mail Sending failure : " + e.getMessage());
            response.setStatus(Boolean.FALSE);
        }
    }

    public void SendConfirmationMail(String destination, String token) {
        Map<String, String> model = new HashMap<>();
        model.put("token", token);

        System.out.print(destination);

        MailResponse response = new MailResponse();
        MimeMessage message = sender.createMimeMessage();

        try {
            MimeMessageHelper helper = new MimeMessageHelper(message, MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
                    StandardCharsets.UTF_8.name());

            Template t = config.getTemplate("confirm-account.ftl");
            String html = FreeMarkerTemplateUtils.processTemplateIntoString(t, model);

            helper.setTo(destination);
            helper.setText(html, true);
            helper.setSubject("E-Mail de vérification de votre compte");
            helper.setFrom(source);
            sender.send(message);

            response.setMessage("mail send to : success");
            response.setStatus(Boolean.TRUE);
            System.out.println(helper);

        } catch (MessagingException | IOException | TemplateException e) {
            response.setMessage("Mail Sending failure : " + e.getMessage());
            response.setStatus(Boolean.FALSE);
        }

    }

}
