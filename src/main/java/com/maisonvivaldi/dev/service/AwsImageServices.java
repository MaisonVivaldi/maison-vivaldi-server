package com.maisonvivaldi.dev.service;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.PutObjectRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Base64;
import java.util.Date;

@Service
@Slf4j
public class AwsImageServices {

    private AmazonS3 s3client;

    @Value("${amazonProperties.bucketName}")
    private String bucketName;

    @Value("${amazonProperties.endPointUrl}")
    private String endpointUrl;

    @Value("${amazonProperties.accessKey}")
    private String accessKey;

    @Value("${amazonProperties.secretKey}")
    private String secretKey;

    @PostConstruct
    private void init() {

        // Init your AmazonS3 credentials using BasicAWSCredentials.
        BasicAWSCredentials credentials = new BasicAWSCredentials(accessKey, secretKey);

        // Start the client using AmazonS3ClientBuilder, here we goes to make a standard cliente, in the
        // region SA_EAST_1, and the basic credentials.
        this.s3client = AmazonS3ClientBuilder.standard()
                .withRegion(Regions.EU_WEST_3)
                .withCredentials(new AWSStaticCredentialsProvider(credentials))
                .build();
    }

    public String uploadFile(String fileContent, String username) {
        String fileUrl = "";
        try {
            if(fileContent != null){
                String base64Data = fileContent.split(",")[1];
                byte[] decodedBytes = Base64.getDecoder().decode(base64Data);

                String fileName = username + "_" + new Date().getTime();
                String extension  = ".jpg";
                String fullFileName = fileName+extension;

                File imageFile = File.createTempFile(fileName, extension, new File("."));
                FileOutputStream out = new FileOutputStream( imageFile );
                out.write( decodedBytes );
                out.close();

                fileUrl = endpointUrl + "/" + fullFileName;
                uploadFileTos3bucket(fullFileName, imageFile);

                Path imagePath = Paths.get(imageFile.getPath());
                Files.delete(imagePath);
            }
        } catch (Exception e) {
            log.error("Image upload failed");
        }
        return fileUrl;
    }

    private void uploadFileTos3bucket(String fileName, File file) {
        s3client.putObject(new PutObjectRequest(bucketName, fileName, file)
                .withCannedAcl(CannedAccessControlList.PublicRead));
    }

    private String generateFileName(File file) {
        return new Date().getTime() + "-" + file.getName().replace(" ", "_");
    }

}
