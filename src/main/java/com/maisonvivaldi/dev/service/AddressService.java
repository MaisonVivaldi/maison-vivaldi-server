package com.maisonvivaldi.dev.service;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.maisonvivaldi.dev.model.Address;
import com.maisonvivaldi.dev.model.Role;
import com.maisonvivaldi.dev.model.User;
import com.maisonvivaldi.dev.repositories.AddressRepository;
import com.maisonvivaldi.dev.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class AddressService {

    public final AddressRepository addressRepository;
    public final UserRepository userRepository;
    public final CheckUserAuthority userAuthority;
    private final Gson gson;

    @Autowired
    public AddressService(AddressRepository addressRepository, UserRepository userRepository, CheckUserAuthority userAuthority) {
        this.addressRepository = addressRepository;
        this.userRepository = userRepository;
        this.userAuthority = userAuthority;
        this.gson = new GsonBuilder()
                .excludeFieldsWithoutExposeAnnotation()
                .create();
    }

    public ResponseEntity<String> checkAllAddressUser(User user) {
        if (user.getRole() == Role.ROLE_ADMIN) {
            List<Address> listAddresses = this.addressRepository.findAll();
            String json = this.gson.toJson(listAddresses);
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(json);
        } else {
            return ResponseEntity
                    .status(HttpStatus.UNAUTHORIZED)
                    .body("User Role UNAUTHORIZED");
        }
    }

    public ResponseEntity<String> checkAddressById(User user, int addressId) {
        if (userAuthority.checkAddressAuthority(user, addressId)) {
            Address address = this.addressRepository.findAddressById(addressId);
            String json = this.gson.toJson(address);
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(json);
        } else {
            return ResponseEntity
                    .status(HttpStatus.UNAUTHORIZED)
                    .body("User Role UNAUTHORIZED");
        }
    }


    public ResponseEntity<Address> createAddress(Address mAddress, User user) {
        mAddress.setUser(user);
        user.setAddress(mAddress);
        userRepository.save(user);
        addressRepository.save(mAddress);
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(mAddress);
    }

    public ResponseEntity<String> updateAddress(int addressId, Address upAddress, User user) {
        if (userAuthority.checkAddressAuthority(user, addressId)) {
            Address oldAddress = this.addressRepository.findAddressById(addressId);
            if (upAddress.getCity() != null) {
                oldAddress.setCity(upAddress.getCity());
            }
            if (upAddress.getCountry() != null) {
                oldAddress.setCountry(upAddress.getCountry());
            }
            if (upAddress.getPostalCode() != null) {
                oldAddress.setPostalCode(upAddress.getPostalCode());
            }
            if (upAddress.getStreet() != null) {
                oldAddress.setStreet(upAddress.getStreet());
            }
            oldAddress.setUpdateDate(new Date());
            addressRepository.save(oldAddress);
            String json = this.gson.toJson(oldAddress);
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(json);
        } else {
            return ResponseEntity
                    .status(HttpStatus.UNAUTHORIZED)
                    .body("User Role UNAUTHORIZED");
        }
    }

    public ResponseEntity<String> deleteAddress(int addressId, User user) {
        if (userAuthority.checkAddressAuthority(user, addressId)) {
            Address address = this.addressRepository.findAddressById(addressId);
            this.addressRepository.delete(address);
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body("{ \"success\" : TRUE }");
        } else {
            return ResponseEntity
                    .status(HttpStatus.UNAUTHORIZED)
                    .body("{ \"success\" : FALSE }");
        }
    }

    public ResponseEntity<?> getAllAddressByUser(User user) {

        List<Address> addresses = this.addressRepository.findAllAddressByUser_Username(user.getUsername());
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(addresses);

    }
}
