package com.maisonvivaldi.dev.service;


import com.maisonvivaldi.dev.model.ImageContent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Slf4j
public class ImageColorService {

    public Map<String, Integer> getImageColor(ImageContent imageContent) throws IOException {
        Map<String, Integer> colorMap = new HashMap<>();
        String base64Data = imageContent.getImageContent().split(",")[1];

        byte[] decodedBytes = Base64.getDecoder().decode(base64Data);
        ByteArrayInputStream bis = new ByteArrayInputStream(decodedBytes);
        BufferedImage image = ImageIO.read(bis);
        for (int r = 0; r < image.getHeight(); r += 1) {
            for (int c = 0; c < image.getWidth(); c += 1) {
                int dataBuffInt = image.getRGB(c, r);
                Color currentColor = new Color(dataBuffInt);
                log.info(currentColor.getTransparency()+"");
                if(currentColor.getTransparency()!=1){
                    log.info(currentColor.getTransparency()+"");
                }
                if (colorMap.get(currentColor.getRed() + "," + currentColor.getGreen() + "," + currentColor.getBlue()) != null) {
                    colorMap.put(currentColor.getRed() + "," + currentColor.getGreen() + "," + currentColor.getBlue()
                            , colorMap.get(currentColor.getRed() + "," + currentColor.getGreen()
                                    + "," + currentColor.getBlue()) + 1);
                } else {
                    colorMap.put(currentColor.getRed() + "," + currentColor.getGreen() + "," + currentColor.getBlue(), 1);
                }
            }
        }

        Map<String, Integer> result = colorMap.entrySet().stream()
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
                        (oldValue, newValue) -> oldValue, LinkedHashMap::new));
        if (imageContent.getImageSize().equals("10mb")) {
            result.entrySet().removeIf(entry -> entry.getValue() < 100000);
        }

        if (imageContent.getImageSize().equals("1mb") ||
            imageContent.getImageSize().equals("500ko")) {
            result.entrySet().removeIf(entry -> entry.getValue() < 10000);
        }

        return result;
    }
}
