package com.maisonvivaldi.dev.service;


import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.maisonvivaldi.dev.model.StanlleyAllProducts;
import com.maisonvivaldi.dev.repositories.StanlleyRepository;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
@Slf4j
public class StanlleyScheduler {

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
    private final StanlleyRepository stanlleyRepository;

    @Autowired
    public StanlleyScheduler(StanlleyRepository stanlleyRepository) {
        this.stanlleyRepository = stanlleyRepository;
    }

    @Scheduled(fixedRate = 43200000)
    public void reportCurrentTime() {
        log.info("The time is now {}", dateFormat.format(new Date()));
        try {
            List<String> gender = new ArrayList<>();
            gender.add("enfants");
            gender.add("unisexe");
            gender.add("hommes");
            gender.add("femmes");
            gender.add("accessoires");

            for (String mGender : gender) {
                URL url = new URL("https://api.stanleystella.com/webrequest/products/get_json");
                HttpURLConnection con = (HttpURLConnection) url.openConnection();
                con.setRequestMethod("POST");
                con.setRequestProperty("Content-Type", "application/json; utf-8");
                con.setRequestProperty("Accept", "application/json");
                con.setDoOutput(true);

                String jsonInputString = "";
                if (!mGender.equals("accessoires")) {
                    jsonInputString = "{" +
                            "\"jsonrpc\": \"2.0\"," +
                            "\"method\": \"call\"," +
                            "\"params\": {" +
                            "\"db_name\": \"production_api\"," +
                            "\"user\": \"info@dollarsanddreams.fr\"," +
                            "\"password\": \"Algerie62\"," +
                            "\"LanguageCode\": \"fr_FR\"," +
                            "\"Gender\": \"" + mGender + "\"" +
                            "}" +
                            "}";
                } else {
                    jsonInputString = "{" +
                            "\"jsonrpc\": \"2.0\"," +
                            "\"method\": \"call\"," +
                            "\"params\": {" +
                            "\"db_name\": \"production_api\"," +
                            "\"user\": \"info@dollarsanddreams.fr\"," +
                            "\"password\": \"Algerie62\"," +
                            "\"LanguageCode\": \"fr_FR\"," +
                            "\"Category\": \"" + mGender + "\"" +
                            "}" +
                            "}";
                }


                OutputStream os = con.getOutputStream();
                byte[] input = jsonInputString.getBytes(StandardCharsets.UTF_8);
                os.write(input, 0, input.length);

                BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), StandardCharsets.UTF_8));

                StringBuilder response = new StringBuilder();
                String responseLine = null;
                while ((responseLine = br.readLine()) != null) {
                    response.append(responseLine.trim());
                }
                String jsonString = response.toString();
                JSONObject obj = new JSONObject(jsonString);
                String result = obj.getString("result");
                JSONArray jsonArray = new JSONArray(result);
                String content = jsonArray.toString();
                final ObjectMapper objectMapper = new ObjectMapper();
                List<StanlleyAllProducts> stanlleyProductDetails = objectMapper.readValue(
                        content,
                        new TypeReference<List<StanlleyAllProducts>>() {
                        });
                stanlleyRepository.saveAll(stanlleyProductDetails);
                con.disconnect();
                log.info(StanlleyScheduler.class+": Stanlley "+mGender+" products updated");
            }
        } catch (
                IOException e) {
            e.printStackTrace();
        }

    }
}
