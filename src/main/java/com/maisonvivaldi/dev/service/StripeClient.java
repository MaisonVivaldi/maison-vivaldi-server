package com.maisonvivaldi.dev.service;

import com.stripe.Stripe;
import com.stripe.model.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
@Slf4j
public class StripeClient {
    @Autowired
    StripeClient() {
        Stripe.apiKey = "sk_test_51HQCNLDjYbdlRkwa9sdpgzMDYZwEmHbyaPA9Gf5n5Dyw5mXpKlDogGVvb2cGszwIrRVJrcXKfHoAbjnNzBlPHc5G00BqM2jrZC";
    }

    public String paymentIntentCreate(int amount, String token) throws Exception {

        Map<String, Object> customerParams = new HashMap<>();
        customerParams.put(
                "description",
                "My First Test Customer (created for API docs)"
        );
        customerParams.put("email", "wazeproo@gmail.com");

        Customer customer = Customer.create(customerParams);

        Map<String, Object> cardParams = new HashMap<>();
        cardParams.put("type", "card");
        cardParams.put("card[token]", token);

        PaymentMethod paymentMethod =
                PaymentMethod.create(cardParams);

        Map<String, Object> customerMapParams = new HashMap<>();
        customerMapParams.put("customer", customer.getId());

        paymentMethod = paymentMethod.attach(customerMapParams);

        List<Object> paymentMethodTypes =
                new ArrayList<>();
        paymentMethodTypes.add("card");
        Map<String, Object> params = new HashMap<>();
        params.put("amount", amount);
        params.put("currency", "eur");
        params.put("customer", customer.getId());
        params.put("payment_method_types",paymentMethodTypes);
        Map<String, Object> cardSecure = new HashMap<>();
        Map<String, Object> card3DSecure = new HashMap<>();
        cardSecure.put("card",card3DSecure);
        card3DSecure.put("request_three_d_secure","any");
        params.put("payment_method_options", cardSecure);

        PaymentIntent paymentIntent =
                PaymentIntent.create(params);

        Map<String, Object> paramsPm = new HashMap<>();
        paramsPm.put("payment_method", paymentMethod.getId());
        paymentIntent.setReceiptEmail(customer.getEmail());

        PaymentIntent updatedPaymentIntent = paymentIntent.confirm(paramsPm);

        String stringPaymentIntent = updatedPaymentIntent.toJson();

        return stringPaymentIntent;
    }
}
