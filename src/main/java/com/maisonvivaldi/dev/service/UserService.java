package com.maisonvivaldi.dev.service;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.maisonvivaldi.dev.model.JwtUserDetails;
import com.maisonvivaldi.dev.model.TokenVerification;
import com.maisonvivaldi.dev.model.User;
import com.maisonvivaldi.dev.repositories.TokenVerificationRepository;
import com.maisonvivaldi.dev.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {
    private final Gson gson;
    public final UserRepository userRepository;
    public final TokenVerificationRepository tokenVerif;
    public final CheckUserAuthority userAuthority;

    @Autowired
    public UserService(UserRepository userRepository, TokenVerificationRepository tokenVerified, CheckUserAuthority userAuthority) {
        this.tokenVerif = tokenVerified;
        this.gson = new GsonBuilder()
                .excludeFieldsWithoutExposeAnnotation()
                .create();
        this.userRepository = userRepository;
        this.userAuthority = userAuthority;
    }

    public ResponseEntity<?> checkAllUsers(User user) {
        List<User> listUser = this.userRepository.findAll();
        String json = this.gson.toJson(listUser);
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(json);
    }

    public ResponseEntity<String> checkUserById(User mUser, int userId) {
            User user = this.userRepository.findByUsername(mUser.getUsername());
            String json = this.gson.toJson(user);
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(json);
    }

    public ResponseEntity<String> updateUser(int userId, User upUser, User oldUser){
        if(userAuthority.checkUserAuthority(oldUser, userId)){
            if(upUser.getUsername()!=null){
                oldUser.setUsername(upUser.getUsername());
            }
            if(upUser.getRole()!=null){
                oldUser.setRole(upUser.getRole());
            }
            String json  = this.gson.toJson(oldUser);
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(json);
        }else{
            return ResponseEntity
                    .status(HttpStatus.UNAUTHORIZED)
                    .body("User Role UNAUTHORIZED");
        }
    }

    public ResponseEntity<List<User>> getAllUser(){
        List<User> lUser = userRepository.findAll();
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(lUser);
    }

    public ResponseEntity<User> getUserByUsername(String username){
        User user = userRepository.findByUsername(username);
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(user);
    }

    public ResponseEntity<String> confirmUser(String token, User user){
        TokenVerification tokenVerification = tokenVerif.findTokenVerificationByToken(token);
        if(tokenVerification != null){
            user.setVerified(true);
            userRepository.save(user);
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body("Save with succes");
        }
        return ResponseEntity
                .status(HttpStatus.EXPECTATION_FAILED)
                .body("Error to set validate account for user");

    }

}
