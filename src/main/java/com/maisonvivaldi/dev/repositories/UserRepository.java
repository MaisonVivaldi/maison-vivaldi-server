package com.maisonvivaldi.dev.repositories;

import com.maisonvivaldi.dev.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {
    User findByUsername(String username);
    List<User> findAll();
    boolean existsUserByUsername(String username);
}
