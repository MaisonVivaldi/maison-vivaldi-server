package com.maisonvivaldi.dev.repositories;

import com.maisonvivaldi.dev.model.Address;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AddressRepository extends CrudRepository<Address, Integer> {
    Address findAddressById(int id);
    Address findAddressByUser_Username(String username);
    List<Address> findAllAddressByUser_Username(String username);
    List<Address> findAll();
}
