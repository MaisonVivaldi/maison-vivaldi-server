package com.maisonvivaldi.dev.repositories;

import com.maisonvivaldi.dev.model.Format;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FormatRepository extends CrudRepository<Format, Integer> {
    Format findFormatByFormatName(String formatName);
    List<Format> findAll();
}
