package com.maisonvivaldi.dev.repositories;

import com.maisonvivaldi.dev.model.Command;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CommandRepository extends CrudRepository<Command, Integer> {
    Command findCommandById(int id);
    List<Command> findAll();
    Command findCommandByInvoiceId(String invoiceId);
}
