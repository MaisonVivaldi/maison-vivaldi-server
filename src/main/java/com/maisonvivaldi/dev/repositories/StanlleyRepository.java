package com.maisonvivaldi.dev.repositories;


import com.maisonvivaldi.dev.model.StanlleyAllProducts;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StanlleyRepository extends CrudRepository<StanlleyAllProducts, Integer> {
    List<StanlleyAllProducts> findStanlleyAllProductsByGenderAndCategoryNot(String gender, String accessoires);
    List<StanlleyAllProducts> findStanlleyAllProductsByCategory(String category);
    List<StanlleyAllProducts> findAll();
    List<StanlleyAllProducts> findById(String id);
    List<StanlleyAllProducts> findStanlleyAllProductsByStyleCode(String styleCode);
    List<StanlleyAllProducts> findStanlleyAllProductsByStyleCodeAndColorCode(String styleCode, String color);
}
