package com.maisonvivaldi.dev.repositories;

import com.maisonvivaldi.dev.model.Product;
import com.maisonvivaldi.dev.model.TokenVerification;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TokenVerificationRepository extends CrudRepository<TokenVerification, Integer> {
    TokenVerification findTokenVerificationByToken(String token);
}
