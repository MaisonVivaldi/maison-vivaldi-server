package com.maisonvivaldi.dev.repositories;

import com.maisonvivaldi.dev.model.Product;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductsRepository extends CrudRepository<Product, Integer> {
    Product findProductById(int productId);
    List<Product> findAll();
}
