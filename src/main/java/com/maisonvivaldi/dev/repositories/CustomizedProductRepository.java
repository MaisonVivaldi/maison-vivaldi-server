package com.maisonvivaldi.dev.repositories;

import com.maisonvivaldi.dev.model.CustomizedProduct;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CustomizedProductRepository extends CrudRepository<CustomizedProduct, Integer> {
    CustomizedProduct findCustomizedProductById(int productId);
    List<CustomizedProduct> findAll();
}
